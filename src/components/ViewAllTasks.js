import React, { Component } from 'react'
import '../style/custom-style'
import TodoCard from './TodoCard'

export default class ViewAllTasks extends Component {

    constructor(props) {
        super(props)
        this.state = {
            edit: false
        }
    }

    handleEdit = () => {
        this.setState(prevState => {
            return {
                edit: !prevState.edit
            }
        })
    }

    render() {
        return (
            <div>
                {!this.state.edit ? (
                    this.props.tasks.map((v, index)=>{
                        return <div className = "whole-card-div">
                                <h2 className = "task-title" 
                                key = {`task-${index}`}>{v.taskname}</h2>
                                {v.todo.map((e, i)=>{
                                    if(e.value != '') return <TodoCard 
                                    value = {e.value} key = {`todocard-${i}`} 
                                    index = {index} todoindex = {i}
                                    handleDeleteTodo = {this.props.handleDeleteTodo}
                                    handleTodoEdit = {this.props.handleTodoEdit}
                                    handleTodoCheckbox = {this.props.handleTodoCheckbox}
                                    handleCheckedState = {this.props.handleCheckedState}/>
                                })}
                                <div>
                                    <button className = "delete-task"
                                    onClick = {()=>this.props.handleDeleteTask(index)}>
                                        Delete task</button>
                                    <button className = "edit-taskname"
                                    onClick = {()=>this.handleEdit()}>
                                        Edit taskname</button>
                                </div>    
                            </div>
                    })
                ): (
                    this.props.tasks.map((v, index)=>{
                        return <div className = "whole-card-div">
                                <input className = "task-title-input" 
                                onBlur = {(e)=>this.props.handleTaskNameEdit(index,
                                    e.target.value)} value = {v.taskname}
                                key = {`task-${index}`}></input>
                                {v.todo.map((e, i)=>{
                                    if(e.value != '') return <TodoCard 
                                    value = {e.value} key = {`todocard-${i}`} 
                                    index = {index} todoindex = {i}
                                    handleDeleteTodo = {this.props.handleDeleteTodo}
                                    handleTodoEdit = {this.props.handleTodoEdit}
                                    handleTodoCheckbox = {this.props.handleTodoCheckbox}
                                    handleCheckedState = {this.props.handleCheckedState}/>
                                })}
                                <div>
                                    <button className = "delete-task"
                                    onClick = {()=>this.handleEdit()}>
                                        Update task</button>
                                </div>    
                            </div>
                    })
                )}
            </div>
        )
    }
}
