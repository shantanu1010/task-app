import React, { Component } from 'react'
import '../style/custom-style'

export default class AddTask extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id : Math.random(),
            taskname : '',
            todo : []
        }
    }

    addNewTodo = (completed = false, value = '') => {
        return {completed, value}
    }

    setTaskName = value => this.setState({taskname : value})

    setTodo = (value, index) => {
        console.log(this.state.todo)
        let temp = this.state.todo
        temp[index].value = value
        this.setState({todo : temp})
    }

    addTodo = () => {
        let ob =  this.addNewTodo()
        this.setState({todo : this.state.todo.concat(ob)})
    }

    setId = () => this.setState({id : Math.random()})

    resetInputValues = () => {
        this.setState({taskname : ''})
        this.setState({todo : []})
    }

    saveTask = () => {
        console.log(this.state)
        // let ar = this.state.todo.filter(singleTodo => singleTodo.value != '')
        // this.setState({todo: ar})
        this.setId()        
        this.props.handleSaveTask(this.state)
        this.resetInputValues()
        //alert('Task saved')
    }

    render() {
        return (
            <div class = "add-task container">
                <form className = "add-task-form">
                    <label>
                        Task Name <input type = "text" name = "task-name"
                        value = {this.state.taskname}
                        onBlur = {(e) => this.setTaskName(e.target.value)}></input>
                    </label>       
                    <h4>List todos </h4>    
                    <div>
                        {this.state.todo.map((v, index) => 
                            <input key = {`todo-${index}`} type = "text" name = "add-todo"
                            onBlur = {(e) => this.setTodo(e.target.value, index)}></input>  
                        )}                               
                        <button className = "add-todo-btn" type = "button"
                        onClick = {() => this.addTodo()}>
                        Add todo</button>  
                    </div>
                    <button type = "button" onClick = {() => this.saveTask()}
                    className = "save-btn">Save</button> 
                </form>
            </div>
        )
    }
}
