import React, { Component } from 'react'
import AddTask from './AddTask'
import ViewAllTasks from './ViewAllTasks'
import '../style/custom-style'

export default class Home extends Component {

    constructor() {
        super();
        this.state = {
            page : '',
            tasks : []
        }
    }

    handlePageChange = value => {
        this.setState(() => {
            return {
                page: value
            }
        })
    }

    handleSaveTask = value => {
        this.setState(prevState => {
            return {
                tasks : prevState.tasks.concat(value)
            }
        })
    }

    handleDeleteTask = index => {
        let ar = this.state.tasks
        ar.splice(index, 1)
        this.setState({tasks: ar})
    }

    handleDeleteTodo = (index, i) => {
        let ar = this.state.tasks[index].todo
        ar.splice(i, 1)
        this.setState(prevState => {
            return {
                ...prevState.tasks,
                todo : ar
            }
        })
    }

    handleTodoEdit = (index, i, value) => {
        let tempTasks = this.state.tasks
        let tempTask = {...this.state.tasks[index]}
        let tempTodoList = tempTask.todo
        let todoTobeUpdated = tempTask.todo[i]
        todoTobeUpdated.value = value
        tempTodoList[i] = todoTobeUpdated
        tempTask.todo = tempTodoList
        tempTasks[index] = tempTask
        this.setState({tasks: tempTasks})
    }

    handleTaskNameEdit = (index, value) => {
        let tempTasks = this.state.tasks
        let tempTask = {...this.state.tasks[index]}
        tempTask.taskname = value
        tempTasks[index] = tempTask
        this.setState({tasks : tempTasks})
    }

    handleTodoCheckbox = (index, i) => {
        let tempTasks = this.state.tasks
        let tempTask = {...this.state.tasks[index]}
        let tempTodoList = tempTask.todo
        let todoCheckboxTobeUpdated = tempTask.todo[i]
        todoCheckboxTobeUpdated.completed = !todoCheckboxTobeUpdated.completed
        tempTodoList[i] = todoCheckboxTobeUpdated
        tempTask.todo = tempTodoList
        tempTasks[index] = tempTask
        this.setState({tasks: tempTasks})
    }

    handleCheckedState = (index, i) => this.state.tasks[index].todo[i].completed

    render() {
        return (
            <div>
                <div className = "home-div">
                    <div>
                        <button className = "add-task-btn"
                        onClick = {() => this.handlePageChange("AddTask")}
                        >Add task</button>
                    </div>
                    <div>
                        <button className = "view-all-tasks-btn"
                        onClick = {() => this.handlePageChange("ViewAllTasks")}>
                            View all tasks</button>
                    </div>
                </div>
                <div className = "toggling-components-div">
                    { this.state.page == "ViewAllTasks" ? <ViewAllTasks 
                    tasks = {this.state.tasks} handleDeleteTask = {this.handleDeleteTask}
                    handleDeleteTodo = {this.handleDeleteTodo}
                    handleTaskNameEdit = {this.handleTaskNameEdit}
                    handleTodoEdit = {this.handleTodoEdit}
                    handleTodoCheckbox = {this.handleTodoCheckbox}
                    handleCheckedState = {this.handleCheckedState}/>
                     : <AddTask tasks = {this.state.tasks}
                    handleSaveTask = {this.handleSaveTask}/>}
                </div>
            </div>
        )
    }
}
