import React, { Component } from 'react'
import '../style/custom-style'

export default class TodoCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            edit: false
        }
    }

    handleEdit = () => {
        this.setState(prevState => {
            return {
                edit: !prevState.edit
            }
        })
    }

    render() {
        return (
            <div className = "todo-card-container">
                {!this.state.edit ? (
                    <div>
                        <h5 className = "todo-desc-h5"
                        >{this.props.value}</h5>
                        <input type = "checkbox"
                        defaultChecked = {this.props.handleCheckedState(
                            this.props.index, this.props.todoindex)}
                        onChange = {() => this.props.handleTodoCheckbox(
                            this.props.index, this.props.todoindex)}>
                        </input>
                        <button className = "delete-todo"
                        onClick = {() => this.handleEdit()}>Edit todo</button>
                        <button className = "delete-todo" 
                        onClick = {() => 
                        this.props.handleDeleteTodo(this.props.index,
                        this.props.todoindex)}>X</button>
                    </div>
                ) : (
                    <div>
                        <input className = "todo-desc-h5"
                        value = {this.props.value}
                        onBlur = {(e) => this.props.handleTodoEdit(
                            this.props.index, this.props.todoindex,
                            e.target.value)}></input>
                        <button className = "delete-todo"
                        onClick = {() => this.handleEdit()}>Update Todo</button>
                    </div>
                )}
            </div>
        )
    }
}
